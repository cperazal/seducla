<?php

namespace Seducla;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    protected $table = "profesors";

    protected $fillable = [
        'codigo', 'nombre', 'correo', 'estatus',
    ];


}
