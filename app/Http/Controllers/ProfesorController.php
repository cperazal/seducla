<?php

namespace Seducla\Http\Controllers;

use Illuminate\Http\Request;
use Seducla\Http\Requests;
use Seducla\Http\Requests\ProfesorCreateRequest;
use Seducla\Http\Requests\ProfesorUpdateRequest;
use \Seducla\Profesor;
use Redirect;
use Session;

class ProfesorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Profesor::All();
        return view('profesor.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profesor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfesorCreateRequest $request)
    {
        Profesor::create([
            'codigo' => $request['codigo'],
            'nombre' => $request['nombre'],
            'correo' => $request['correo'],
            'estatus' => true,
            ]);

        return redirect('/profesor')->with('message', 'store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profesor = Profesor::find($id);
        return view('profesor.edit', ['profesor' => $profesor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfesorUpdateRequest $request, $id)
    {
        $profesor = Profesor::find($id);
        $profesor->fill($request->all());
        $profesor->save();

        Session::flash('message', 'Profesor editado exitosamente');
        return Redirect::to('/profesor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profesor::destroy($id);
        Session::flash('message', 'Profesor eliminado exitosamente');
        return Redirect::to('/profesor');

    }
}
