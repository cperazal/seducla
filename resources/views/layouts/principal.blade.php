<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Seducla</title>
  <!-- Bootstrap core CSS-->

  {!!Html::style('vendor/bootstrap/css/bootstrap.min.css')!!}
  {!!Html::style('vendor/font-awesome/css/font-awesome.min.css')!!}
  {!!Html::style('vendor/datatables/dataTables.bootstrap4.css')!!}
  {!!Html::style('css/sb-admin.css')!!}
  
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="index.html">Seducla</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Cursos</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="{!!URL::to('/curso')!!}">Ver cursos</a>
            </li>
            <li>
              <a href="{!!URL::to('/curso/create')!!}">Nuevo curso</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Profesores</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="{!!URL::to('/profesor')!!}">Ver profesores</a>
            </li>
            <li>
              <a href="{!!URL::to('/profesor/create')!!}">Nuevo profesor</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text">Departamentos</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="{!!URL::to('/departamento')!!}">Ver departamentos</a>
            </li>
            <li>
              <a href="{!!URL::to('/departamento/create')!!}">Nuevo departamento</a>
            </li>
          </ul>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Salir</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">

    <!--Contenido de la vista-->
    @yield('content')
    <!--/.Contenido de la vista-->

  </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Seducla 2018</small>
        </div>
      </div>
    </footer>


    <!-- Bootstrap core JavaScript-->
    {!!Html::script('vendor/jquery/jquery.min.js')!!}
    {!!Html::script('vendor/bootstrap/js/bootstrap.bundle.min.js')!!}
    <!-- Core plugin JavaScript-->
    {!!Html::script('vendor/jquery-easing/jquery.easing.min.js')!!}
    <!-- Page level plugin JavaScript-->
    {!!Html::script('vendor/chart.js/Chart.min.js')!!}
    {!!Html::script('vendor/datatables/jquery.dataTables.js')!!}
    {!!Html::script('vendor/datatables/dataTables.bootstrap4.js')!!}
    <!-- Custom scripts for all pages-->
    {!!Html::script('js/sb-admin.min.js')!!}
    <!-- Custom scripts for this page-->
    {!!Html::script('js/sb-admin-datatables.min.js')!!}
    {!!Html::script('js/sb-admin-charts.min.js')!!}
  
</body>

</html>      