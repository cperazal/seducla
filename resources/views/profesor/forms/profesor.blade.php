<div class="form-group">
	{!!Form::label('Cedula')!!}
	{!!Form::text('codigo',null,['class' => 'form-control', 'placeholder' => 'Ingrese la cedula del profesor'])!!}
</div>
<div class="form-group">
	{!!Form::label('Nombre')!!}
	{!!Form::text('nombre',null,['class' => 'form-control', 'placeholder' => 'Ingrese el nombre del profesor'])!!}
</div>
<div class="form-group">
	{!!Form::label('Correo')!!}
	{!!Form::text('correo',null,['class' => 'form-control', 'placeholder' => 'Ingrese el correo del profesor'])!!}
</div>
<div class="form-group">
	{!!Form::label('Departamento')!!}
	{!!Form::select('departamento', ['M' => 'Matematica', 'F' => 'Fisica'], 'S')!!}
</div>