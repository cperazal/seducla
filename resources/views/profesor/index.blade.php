@extends('layouts.principal')

<?php
	$message=Session::get('message');
?>

@section('content')

@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	  {{Session::get('message')}}
	</div>	
@endif

	<div class="card-header">
  		<i class="fa fa-user"></i> Lista de profesores
  	</div>
	<div class="card-body">
		<table class="table">
			<thead>
				<th>Codigo</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Operación</th>
			</thead>
			@foreach($users as $user)
			<tbody>
				<td>{{$user->codigo}}</td>
				<td>{{$user->nombre}}</td>
				<td>{{$user->correo}}</td>
				<td>
					{!!link_to_route('profesor.edit', $title = "Editar", $parameters = $user->id, $attributes = ['class' => 'btn btn-primary']);!!}
				</td>
			</tbody>
			@endforeach
		</table>
	</div>	

@stop

