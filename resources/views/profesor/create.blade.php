@extends('layouts.principal')

@section('content')
	
	@include('alerts.request')

	<div class="card-header">
  		<i class="fa fa-user"></i> Registrar profesor
  	</div>
	<div class="card-body">

		{!!Form::open(['route' => 'profesor.store', 'method' => 'POST'])!!}
			@include('profesor.forms.profesor')
			{!!Form::submit('Guardar',['class' => 'btn btn-primary'])!!}
		{!!Form::close()!!}


	</div>
			
	
@stop