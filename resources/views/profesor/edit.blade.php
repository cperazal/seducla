@extends('layouts.principal')

@section('content')

	<div class="card-header">
  		<i class="fa fa-user"></i> Editar profesor
  	</div>

  	@include('alerts.request')

	<div class="card-body">
		{!!Form::model($profesor, ['route' => ['profesor.update', $profesor->id], 'method' => 'PUT'])!!}
			@include('profesor.forms.profesor')
			{!!Form::submit('Guardar',['class' => 'btn btn-primary'])!!}			
		{!!Form::close()!!}

		{!!Form::open(['route' => ['profesor.destroy', $profesor->id], 'method' => 'DELETE'])!!}
				{!!Form::submit('Eliminar',['class' => 'btn btn-danger'])!!}
		{!!Form::close()!!}

	</div>	

@stop