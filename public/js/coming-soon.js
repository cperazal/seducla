(function($) {
  "use strict"; // Start of use strict

  // Vide - Video Background Settings
  $('body').vide({
    poster: "img/laptop.jpg"
  }, {
    posterType: 'jpg'
  });

})(jQuery); // End of use strict
